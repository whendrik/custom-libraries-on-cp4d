# Custom Libraries on CP4D


![frigidum](https://gitlab.com/whendrik/custom-libraries-on-cp4d/-/raw/master/images/custom_env.gif)


Example of a .condarc file to use custom libraries on CP4D

 - **!** Make sure to have both `dependencies:` uncommented, and `  - pip:` when adding packages with `pip`.

```
# Please modify the following content to add a software customization for an environment.
# To remove an existing software customization please remove the entire content and click apply.

# Please add conda channels here indented by two spaces and a hyphen.
channels:
  - defaults

# Please add conda packages here intented by two spaces and a hyphen.
# Example:
# dependencies:
#  - a_conda_package=1.0
#
# To add conda packages, please un-comment the next line below.

dependencies:

# Please add pip packages here.
# List your pip packages indented by four spaces and a hyphen.
# Example:
#  -pip:
#    - a_pip_package==1.0
#
# To add pip packages, please un-comment the next line below.
  - pip:
     - heamy
     - tsfresh
     - feature-stuff
     - tpot
```
